/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Button,
  Alert,
  NativeModules,
  NativeEventEmitter,
  DeviceEventEmitter
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

const TYImageSDK = NativeModules.TyImageSdk;
const PayModule = NativeModules.PayModule;

// 封装组件
class Hellow extends React.Component{

  constructor(props){
      super(props);
      // 设置初始透明度为1
      // this.state = {opacity:1.0}
      this.state = {
        isLoading: false,
        isStartDiscover: false,
        alpha: 0.7,
        showResult: false,
        resultImage: '',
      };
  }
  
  onStartPay=()=>{
    let obj={
      orderId:'11485',
      tradeNo:'wqe',
      result:'pending',
      token:'123'
    };
    PayModule.payNotify(obj)
  }
  
  onStartMemberList(){
    PayModule.showOrder();
    // Alert.alert('showOrder')
  }
  
  componentDidMount() {
    this.eventEmitter = new NativeEventEmitter(TYImageSDK);
    this.eventPayEmitter = new NativeEventEmitter(PayModule);
    this.eventEmitter.addListener('PhotoEditorDidSave', this.onSaveImage);
    this.eventEmitter.addListener('StartShowOrder', this.onStartMemberList);

    this.eventPayEmitter.addListener('StartPay', this.onStartPay);

    setTimeout( () => {
      SplashScreen.hide();
    }, 1000);
  }

  componentWillUnmount() {
    if (this.eventEmitter) {
      this.eventEmitter.removeListener('PhotoEditorDidSave', this.onSaveImage);
      this.eventEmitter.removeListener('StartShowOrder', this.onStartMemberList);
    }
    if(this.eventPayEmitter){
      this.eventPayEmitter.removeListener('StartPay', this.onStartPay)
    }
  }

  _intoSdk(){

    let obj={
      path:'/storage/emulated/0/Pictures/1622616291365.jpg',
      exportDir:'',
      userId:"37", 
      token:"123",
      username:"fdg",
      isMember:false,
      memberExpiredAt:"tqrt43524",
      avatar:"http:4354254",
      pageType:""
    };
    NativeModules.TyImageSdk.present(obj)
    
    }

  render(){
      return(
        <SafeAreaView style={Colors.lighter}>
      
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={Colors.lighter}>
        <Header />
        <View
          style={{
            backgroundColor: Colors.white ,
          }}>
            <Button title={"单击"} onPress={() => {
                    this._intoSdk()
                }} />
          
        </View>
        
      </ScrollView>
     </SafeAreaView>
      );
  }
}

export default Hellow;
